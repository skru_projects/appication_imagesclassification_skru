import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ScanPage extends StatefulWidget {
  const ScanPage({super.key});

  @override
  State<ScanPage> createState() => _ScanPageState();
}

class _ScanPageState extends State<ScanPage> {
  late Size mediaSize;
  final List<int> mapValue = [1, 2];
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Stack(
          children: [
            Container(
              height: 600,
              width: double.infinity,
              decoration: BoxDecoration(color: Colors.amber[50]),
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.only(top: 70.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/images/main.png',
                        scale: 2,
                        opacity: const AlwaysStoppedAnimation(.8),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 420.0),
              child: Container(
                height: 300,
                width: double.infinity,
                decoration: const BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black26, spreadRadius: 3, blurRadius: 3)
                    ],
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20))),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    children: [
                      const Text(
                        'Lorem ipsum dolor',
                        style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                            color: Colors.black54),
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 40.0),
                        child: Text(
                          'Lorem ipsum dolor sit amet consectetur. Dui urna ',
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: Colors.black38),
                        ),
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                      ButtonMain(
                        title: 'Camera',
                        icon: const FaIcon(
                          FontAwesomeIcons.camera,
                          color: Colors.black38,
                        ),
                        onClick: () => {print('Camera')},
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      ButtonMain(
                        title: 'Album',
                        icon: const FaIcon(
                          FontAwesomeIcons.image,
                          color: Colors.black38,
                        ),
                        onClick: () => {print('Album')},
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        )
      ],
    );
  }
}

class ButtonMain extends StatelessWidget {
  final String title;
  final FaIcon icon;
  final Function onClick;
  const ButtonMain({
    super.key,
    required this.title,
    required this.icon,
    required this.onClick,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      height: 50,
      decoration: BoxDecoration(
          color: Colors.amber[50],
          borderRadius: BorderRadius.circular(16),
          border: Border.all(width: 2, color: Colors.black38)),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            icon,
            const SizedBox(
              width: 10,
            ),
            Text(
              title,
              style: const TextStyle(
                  fontSize: 18,
                  color: Colors.black26,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );
  }
}
